# gitlab-load-generator

Requires Python 3.x, pip, virtualenv

```
# Create a virtual environment
virtualenv .env

# Activate the virtual environment
source .env/bin/activate # Linux/Mac
# or
.env\Scripts\activate # Windows

# Install dependencies
pip install -r requirements.txt

# help
python genload.py --help

# start running
python genload.py --token MY_TOKEN --gitlab-addr https://some.gitlab.server
```