import argparse
import gitlab
import threading
import time
import petname


class Program:
    args = None
    parser = argparse.ArgumentParser(description="Generates load on a GitLab server")

    @staticmethod
    def main():
        program = Program()
        program.run()

    def run(self):
        self.add_arguments_and_parse()
        print(f"Using gitlab: {self.args.gitlab_addr} and token {self.args.token}")
        gl = gitlab.Gitlab(self.args.gitlab_addr, private_token=self.args.token)

        while True:
            threading.Thread(target=create_and_delete_repo, args=[gl]).start()
            threading.Thread(target=load_projects, args=[gl]).start()
            threading.Thread(target=load_runners, args=[gl]).start()
            threading.Thread(target=load_groups, args=[gl]).start()
            time.sleep(1)

    def add_arguments_and_parse(self):
        self.parser.add_argument("--token", dest="token", required=True,
                                 help="The gitlab token to use")
        self.parser.add_argument("--gitlab-addr", dest="gitlab_addr", required=True,
                                 help="The gitlab server to use")
        self.args = self.parser.parse_args()


def create_and_delete_repo(gl):
    project_name = f"gl-load-{petname.generate()}"
    print(f"creating then deleting project: {project_name}")
    project = gl.projects.create({'name': project_name})
    gl.projects.delete(project.attributes['id'])


def load_projects(gl):
    print("loading all projects")
    for project in gl.projects.list(all=True):
        print(project)


def load_runners(gl):
    print("loading all runners")
    for runner in gl.runners.all(per_page=100):
        print(f"found runner id: {runner.attributes['id']} contacted at: {runner.contacted_at}")


def load_groups(gl):
    print("loading all groups")
    for group in gl.groups.list(all=True):
        print(group)


if __name__ == "__main__":
    Program.main()
